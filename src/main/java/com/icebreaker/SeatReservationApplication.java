package com.icebreaker;

import com.icebreaker.ticket.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.icebreaker.ticket.repository.SeatReserveRepository;

import java.util.Date;

@SpringBootApplication
public class SeatReservationApplication implements CommandLineRunner {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	SeatReserveRepository repository;

	@Autowired
	TicketService ticketService;

	public static void main(String[] args) {
		SpringApplication.run(SeatReservationApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {


	}
}
