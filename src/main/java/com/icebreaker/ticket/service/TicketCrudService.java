package com.icebreaker.ticket.service;

import com.icebreaker.ticket.entity.SeatHoldDetail;

public interface TicketCrudService {

    /**
     * Insert hold record in seat_hold_detail table
     *
     * @param customerEmail - unique identifier for holder
     * @param numSeats - number of seats to be held
     * @return - the hold record
     */
    SeatHoldDetail addHoldRecord(String customerEmail, int numSeats);

    /**
     * Due to holding competition, customer failed to hold all required seats.
     * customer calls reset function to release the partial holding seats
     *
     * @param seatHoldId - unique identifier for hold record
     * @return - the number of partial holding seats
     */
    int resetHoldSeats(int seatHoldId);

    /**
     * Customer calls this function to hold the seats.
     *
     * @param seatHoldId - unique identifier for hold record
     * @param numSeats - the expected number of seats to be held
     * @param expirationSeconds - the expiration seconds after holding
     * @return - the actual holding seat number
     */
    int holdSeats(int seatHoldId, int numSeats, int expirationSeconds);

    /**
     * Reserve seats after holding the seats
     *
     * @param seatHoldId - unique identifier for hold record
     * @param customerEmail - unique identifier for customer
     * @param reservedCode - unique reserve code for customer to track the reservation
     * @return - the number of seats to be reserved
     */
    int reserveSeats(int seatHoldId, String customerEmail, String reservedCode);
}
