package com.icebreaker.ticket.service;

import com.icebreaker.ticket.constant.HoldStatus;
import com.icebreaker.ticket.dto.SeatHold;
import com.icebreaker.ticket.entity.SeatHoldDetail;
import com.icebreaker.ticket.entity.SeatReserve;
import com.icebreaker.ticket.exception.TicketException;
import com.icebreaker.ticket.repository.SeatReserveRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;


@Service
public class TicketServiceImpl implements TicketService {

    @Autowired
    SeatReserveRepository seatReserveRepository;

    @Autowired
    TicketCrudService ticketCrudService;

    @Value("${ticket.expiration.seconds}")
    private Integer expirationSeconds;

    @Override
    public int numSeatsAvailable() {
        return seatReserveRepository.countAvailableSeats();
    }

    @Override
    public SeatHold findAndHoldSeats(int numSeats, String customerEmail) {
        if (expirationSeconds == null)
            throw new TicketException("Expiration seconds value is NOT an integer.");

        if (numSeats <= 0)
            throw new TicketException("Number of seats can't be 0 or negative number");

        if (StringUtils.isBlank(customerEmail))
            throw new TicketException("Customer email is required to hold the seats");

        SeatHold seatHold = new SeatHold();
        seatHold.setCustomerEmail(customerEmail);
        int availableSeats = this.numSeatsAvailable();
        if (availableSeats < numSeats) {
            seatHold.setHoldstatus(HoldStatus.HOLD_FAILED_NO_ENOUGH_SEATS);
        } else {
            //add a seat hold detail record
            SeatHoldDetail seatHoldDetail = ticketCrudService.addHoldRecord(customerEmail, numSeats);
            int seatHoldId = seatHoldDetail.getId().intValue();
            seatHold.setId(seatHoldId);
            seatHold.setHoldTime(seatHoldDetail.getHoldTime());
            seatHold.setCustomerEmail(customerEmail);
            //hold the seats
            int holdingSeatNumber = ticketCrudService.holdSeats(seatHoldId, numSeats, expirationSeconds);
            if (holdingSeatNumber == numSeats) {
                seatHold.setHoldstatus(HoldStatus.HOLD_SUCCESS);
                List<SeatReserve> seats = seatReserveRepository.findByLastHoldId(seatHoldId);
                for (SeatReserve seat : seats)
                    seatHold.getSeatIds().add(seat.getSeatId().intValue());

            } else {
                seatHold.setHoldstatus(HoldStatus.HOLD_FAILED_PARTIAL_SEATS);
                ticketCrudService.resetHoldSeats(seatHoldId);
            }
        }
        return seatHold;
    }


    @Override
    public String reserveSeats(int seatHoldId, String customerEmail) {
        String reservedCode = generateReservedCode();
        int updateCount = ticketCrudService.reserveSeats(seatHoldId, customerEmail, reservedCode);
        return (updateCount <= 0) ? null : reservedCode;
    }

    private String generateReservedCode() {
        return UUID.randomUUID().toString();
    }
}
