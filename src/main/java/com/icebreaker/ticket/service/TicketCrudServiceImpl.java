package com.icebreaker.ticket.service;

import com.icebreaker.ticket.constant.HoldStatus;
import com.icebreaker.ticket.dto.SeatHold;
import com.icebreaker.ticket.entity.SeatHoldDetail;
import com.icebreaker.ticket.entity.SeatReserve;
import com.icebreaker.ticket.exception.TicketException;
import com.icebreaker.ticket.repository.SeatHoldDetailRepository;
import com.icebreaker.ticket.repository.SeatReserveRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.UUID;


@Service
public class TicketCrudServiceImpl implements TicketCrudService {

    @Autowired
    SeatReserveRepository seatReserveRepository;

    @Autowired
    SeatHoldDetailRepository seatHoldDetailRepository;

    @Transactional
    @Override
    public int resetHoldSeats(int seatHoldId) {
        return seatReserveRepository.resetHoldSeats(seatHoldId);
    }

    @Transactional
    @Override
    public int holdSeats(int seatHoldId, int numSeats, int expirationSeconds) {
        return seatReserveRepository.holdSeats(seatHoldId, numSeats, expirationSeconds);
    }

    @Transactional
    @Override
    public SeatHoldDetail addHoldRecord(String customerEmail, int numSeats) {
        SeatHoldDetail seatHoldDetail = new SeatHoldDetail(customerEmail, numSeats);
        return seatHoldDetailRepository.save(seatHoldDetail);
    }

    @Transactional
    @Override
    public int reserveSeats(int seatHoldId, String customerEmail, String reservedCode) {
        return seatReserveRepository.reserveSeats(seatHoldId, customerEmail, reservedCode);
    }

}
