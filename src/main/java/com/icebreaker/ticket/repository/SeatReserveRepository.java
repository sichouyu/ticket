package com.icebreaker.ticket.repository;

import com.icebreaker.ticket.entity.SeatReserve;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface SeatReserveRepository extends JpaRepository<SeatReserve, Long>{

    /**
     *
     * @return the number of available seats for holding
     */
    @Query(value = "select count(*) from seat_reserve sr where (sr.last_hold_expiration_time is null or sr.last_hold_expiration_time <= now()) and sr.reserved_code is null", nativeQuery = true)
    int countAvailableSeats();

    /**
     *
     * @param holdId - unique identifier for holding transaction
     * @return a list of the seats for the holding transaction
     */
    List<SeatReserve> findByLastHoldId(int holdId);

    /**
     *
     * @param holdId - the unique identifier for holding transaction
     * @param customerEmail - the unique identifier to represent holder
     * @param reservedCode - the unique value for customer to keep as reservation record
     * @return number of reserved seats
     */
    @Modifying(clearAutomatically = true)
    @Query(value = "update seat_reserve sr set sr.reserved_by =:customerEmail, sr.reserved_time=now(), sr.reserved_code=:reservedCode where sr.last_hold_id =:holdId and sr.last_hold_expiration_time is not null and sr.last_hold_expiration_time>now() and sr.reserved_by is null", nativeQuery = true)
    int reserveSeats(@Param("holdId") int holdId, @Param("customerEmail") String customerEmail, @Param("reservedCode") String reservedCode);

    /**
     *
     * @param holdId - the unique identifier to refer to seat_hold_detail record
     * @param numberOfSeats - the number of seats are held
     * @param holdSeconds - the number of seconds to expire the holding
     * @return holdResult - how many seats are held for this transaction
     */
    @Modifying(clearAutomatically = true)
    @Query(value = "update seat_reserve sr set sr.last_hold_id =:holdId, sr.last_hold_expiration_time=DATEADD('SECOND',:holdSeconds, NOW()) where " +
            "sr.id in (select sr1.id from seat_reserve sr1 where (sr1.last_hold_expiration_time is null or sr1.last_hold_expiration_time<=now()) and sr1.reserved_code is null " +
            "order by priority desc limit :numberOfSeats)", nativeQuery = true)
    int holdSeats(@Param("holdId") int holdId, @Param("numberOfSeats") int numberOfSeats, @Param("holdSeconds") int holdSeconds);


    /**
     *
     * @param holdId - the unique identifier for the holding transaction
     * @return the number of seats which were reset
     */
    @Modifying(clearAutomatically = true)
    @Query(value = "update seat_reserve sr set sr.last_hold_id = null, sr.last_hold_expiration_time = null where sr.last_hold_id=:holdId", nativeQuery = true)
    int resetHoldSeats(@Param("holdId") int holdId);

}
