package com.icebreaker.ticket.repository;

import com.icebreaker.ticket.entity.SeatHoldDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeatHoldDetailRepository extends JpaRepository<SeatHoldDetail, Long>{
}
