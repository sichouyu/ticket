package com.icebreaker.ticket.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="seat_hold_detail")
public class SeatHoldDetail {
	@Id
	@SequenceGenerator(name="SEAT_HOLD_DETAIL_SEQ", sequenceName="seq_seat_hold_detail",allocationSize=1)
	@GeneratedValue (strategy = GenerationType.TABLE, generator="SEAT_HOLD_DETAIL_SEQ")
	private Long id;

	@Column (name="customer_email", nullable = false)
	private String customerEmail;

	@Column (name="number_of_seats", nullable = false)
	private Integer numberOfSeats;

	@CreationTimestamp
	@Column (name="hold_time", nullable = false)
	private Date holdTime;

	public SeatHoldDetail() {
		super();
	}

	public SeatHoldDetail(String customerEmail, Integer numberOfSeats) {
		super();
		this.customerEmail = customerEmail;
		this.numberOfSeats = numberOfSeats;
	}

	public SeatHoldDetail(Integer id, String customerEmail, Integer numberOfSeats) {
		super();
		this.id = new Long(id);
		this.customerEmail = customerEmail;
		this.numberOfSeats = numberOfSeats;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCustomerEmail() {
		return customerEmail;
	}

	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}

	public Date getHoldTime() {
		return holdTime;
	}

	public void setHoldTime(Date holdTime) {
		this.holdTime = holdTime;
	}

	public Integer getNumberOfSeats() {
		return numberOfSeats;
	}

	public void setNumberOfSeats(Integer numberOfSeats) {
		this.numberOfSeats = numberOfSeats;
	}

	@Override
	public String toString() {
		return String.format("SeatHoldDetail [id=%s, numberOfSeats=%s, customerEmail=%s, holdTime=%s]",
				id, numberOfSeats, customerEmail, holdTime);
	}

}
