package com.icebreaker.ticket.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="seat_reserve")
public class SeatReserve {
	@Id
	@SequenceGenerator(name="SEAT_RESERVE_SEQ", sequenceName="seq_seat_reserve",allocationSize=1)
	@GeneratedValue (strategy = GenerationType.TABLE, generator="SEAT_RESERVE_SEQ")
	private Long id;

	@Column (name="seat_id", nullable = false)
	private Integer seatId;

	@Column (nullable = false)
	private Integer priority;

	@Column (name="last_hold_expiration_time")
	private Date lastHoldExpirationTime;

	@Column (name="last_hold_id")
	private Integer lastHoldId;

	@Column (name="reserved_by")
	private String reservedBy;

	@Column (name="reserved_code")
	private String reservedCode;

	@Column (name="reserved_time")
	private Date reservedTime;

	public SeatReserve() {
		super();
	}

	public SeatReserve(int seatId, int priority, Date lastHoldExpirationTime, Integer lastHoldId, Date reservedTime, String reservedBy, String reservedCode) {
		super();
		this.seatId = seatId;
		this.priority = priority;
		this.lastHoldId = lastHoldId;
		this.lastHoldExpirationTime = lastHoldExpirationTime;
		this.reservedBy = reservedBy;
		this.reservedTime = reservedTime;
		this.reservedCode = reservedCode;
	}

	public SeatReserve(int id, int seatId, int priority, Date lastHoldExpirationTime, Integer lastHoldId, Date reservedTime, String reservedBy, String reservedCode) {
		this(seatId, priority, lastHoldExpirationTime, lastHoldId, reservedTime, reservedBy, reservedCode);
		this.id = new Long(id);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getSeatId() {
		return seatId;
	}

	public void setSeatId(Integer seatId) {
		this.seatId = seatId;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Date getLastHoldExpirationTime() {
		return lastHoldExpirationTime;
	}

	public void setLastHoldExpirationTime(Date lastHoldExpirationTime) {
		this.lastHoldExpirationTime = lastHoldExpirationTime;
	}

	public Integer getLastHoldId() {
		return lastHoldId;
	}

	public void setLastHoldId(Integer lastHoldId) {
		this.lastHoldId = lastHoldId;
	}

	public String getReservedBy() {
		return reservedBy;
	}

	public void setReservedBy(String reservedBy) {
		this.reservedBy = reservedBy;
	}

	public String getReservedCode() {
		return reservedCode;
	}

	public void setReservedCode(String reservedCode) {
		this.reservedCode = reservedCode;
	}

	public Date getReservedTime() {
		return reservedTime;
	}

	public void setReservedTime(Date reservedTime) {
		this.reservedTime = reservedTime;
	}

	@Override
	public String toString() {
		return String.format("SeatReserve [id=%s, seatId=%s, priority=%s, lastHoldId=%s, lastHoldExpirationDate=%s, reservedBy=%s, reservedTime=%s, reservedCode=%s]",
				id, seatId, priority, lastHoldId, lastHoldExpirationTime, reservedBy, reservedTime, reservedCode);
	}

}
