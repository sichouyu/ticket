package com.icebreaker.ticket.constant;

public enum HoldStatus {
    HOLD_SUCCESS("Seat holding finished successfully"),
    HOLD_FAILED_NO_ENOUGH_SEATS("Seat holding failed because there are no enough seats"),
    HOLD_FAILED_COMMIT("Seat holding failed because updating seat_reserve table failed"),
    HOLD_FAILED_PARTIAL_SEATS("Seat holding failed because only partial seats were held successfully");

    private final String type;

    HoldStatus(final String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }

    public static HoldStatus getHoldStatus(String propName){
        for(HoldStatus holdStatus: HoldStatus.values()){
            if(holdStatus.type.equals(propName)){
                return holdStatus;
            }
        }
        return null;
    }
}
