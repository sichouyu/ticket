package com.icebreaker.ticket.dto;

import com.icebreaker.ticket.constant.HoldStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SeatHold {
    private Integer id;
    private List<Integer> seatIds = new ArrayList<>();
    private String customerEmail;
    private Date holdTime;
    private HoldStatus holdstatus;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public Date getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(Date holdTime) {
        this.holdTime = holdTime;
    }

    public List<Integer> getSeatIds() {
        return seatIds;
    }

    public void setSeatIds(List<Integer> seatIds) {
        this.seatIds = seatIds;
    }

    public HoldStatus getHoldstatus() {
        return holdstatus;
    }

    public void setHoldstatus(HoldStatus holdstatus) {
        this.holdstatus = holdstatus;
    }

    @Override
    public String toString() {
        return String.format("SeatHold [id=%s, customerEmail=%s, holdTime=%s, holdStatus=%s, seatIds=%s]",
                id, customerEmail, holdTime, holdstatus, seatIds.toString());
    }
}
