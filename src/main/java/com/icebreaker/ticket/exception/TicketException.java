package com.icebreaker.ticket.exception;

public class TicketException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public TicketException(String message) {
        super(message);
    }
}
