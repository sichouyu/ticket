package com.icebreaker.ticket.controller;

import com.icebreaker.ticket.constant.HoldStatus;
import com.icebreaker.ticket.dto.SeatHold;
import com.icebreaker.ticket.entity.SeatHoldDetail;
import com.icebreaker.ticket.entity.SeatReserve;
import com.icebreaker.ticket.repository.SeatReserveRepository;
import com.icebreaker.ticket.service.TicketService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;

@Controller
public class TicketController {

    @Autowired
    private TicketService ticketService;

    @Autowired
    SeatReserveRepository repository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/ticket/demo", produces = "text/html;charset=UTF-8")
    @ResponseBody
    public String holdSeat() {
        StringBuffer sb = new StringBuffer();
        sb.append("Before the demo: <br><font size=\"3\" color=\"blue\">");
        List<SeatReserve> seatReserves = repository.findAll();
        generateAllRecordHtml(sb, seatReserves);
        sb.append("</font>");

        sb.append("<br> Number of available seats : " + ticketService.numSeatsAvailable());
        sb.append("<br> Customer tries to hold two seats. The customer's email is ticket@icebreaker.com");
        SeatHold seatHold = ticketService.findAndHoldSeats(2, "ticket@icebreaker.com");
        if (seatHold.getHoldstatus().equals(HoldStatus.HOLD_SUCCESS)) {
            sb.append("<br>Two seats are successfully held : " + seatHold.getSeatIds() + " at " + seatHold.getHoldTime()
                    + " with seat hold id " + seatHold.getId());

            sb.append("<br>Customer reserves the seats which he just held");
            String reservedCode = this.ticketService.reserveSeats(seatHold.getId(), "ticket@icebreaker.com");
            sb.append("<br>Reserved code is " + reservedCode);
        } else
            sb.append("<br>Customer failed to hold the seats due to less than 2 available seats.");

        sb.append("<br><br>");
        sb.append("After the demo: <br><font size=\"3\" color=\"blue\">");
        seatReserves = repository.findAll();
        generateAllRecordHtml(sb, seatReserves);
        sb.append("</font>");
        return sb.toString();
    }

    private void generateAllRecordHtml(StringBuffer sb, List<SeatReserve> seatReserves) {
        for (SeatReserve seatReserve:seatReserves) {
            sb.append("Seat ID: " + seatReserve.getSeatId() + " | ");
            sb.append("Priority: " + seatReserve.getPriority() + " | ");
            sb.append("Last Hold ID: " + seatReserve.getLastHoldId() + " | ");
            if (null != seatReserve.getLastHoldId()) {
                sb.append("Last Hold Expiration Time: " + seatReserve.getLastHoldExpirationTime() + " | ");
                if (null != seatReserve.getReservedBy()) {
                    sb.append("Reserved By: " + seatReserve.getReservedBy() + " | ");
                    sb.append("Reserved Time: " + seatReserve.getReservedTime() + " | ");
                    sb.append("Reserved Code: " + seatReserve.getReservedCode() + " | ");
                }
            }
            sb.append("<br>");
        }
    }
}
