insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
values(nextval('seq_seat_hold_detail'), 'allen@icebreaker.com', now(), 1);

insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
values(nextval('seq_seat_hold_detail'), 'steve@icebreaker.com', now(), 1);

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10001, 1, null, null, null, null, null);

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10002, 2, 1, now(), null, null, null);

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10003, 3, 2, now(), now(), 'steve@icebreaker.com', '20a01714-6d1b-420d-bb57-f5aa0d692ba8');

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10004, 4, null, null, null, null, null);