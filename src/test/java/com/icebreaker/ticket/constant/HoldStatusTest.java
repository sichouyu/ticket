package com.icebreaker.ticket.constant;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class HoldStatusTest {

    @Test
    public void getHoldStatus() {
//        Test case: valid hold status
        Assert.assertEquals(HoldStatus.HOLD_SUCCESS, HoldStatus.getHoldStatus(HoldStatus.HOLD_SUCCESS.toString()));

//        Test case: invalid hold status
        Assert.assertNull(HoldStatus.getHoldStatus("Unknown status"));

    }
}