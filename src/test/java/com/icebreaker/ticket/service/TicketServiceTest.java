package com.icebreaker.ticket.service;

import com.icebreaker.SeatReservationApplication;
import com.icebreaker.ticket.constant.HoldStatus;
import com.icebreaker.ticket.dto.SeatHold;
import com.icebreaker.ticket.entity.SeatReserve;
import com.icebreaker.ticket.exception.TicketException;
import com.icebreaker.ticket.repository.SeatHoldDetailRepository;
import com.icebreaker.ticket.repository.SeatReserveRepository;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatReservationApplication.class)
public class TicketServiceTest {

//    insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
//    values(nextval('seq_seat_hold_detail'), 'allen@icebreaker.com', now(), 1);
//
//    insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
//    values(nextval('seq_seat_hold_detail'), 'steve@icebreaker.com', now(), 1);
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10001, 1, null, null, null, null, null);
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10002, 2, 1, now(), null, null, null);
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10003, 3, 2, now(), now(), 'steve@icebreaker.com', '20a01714-6d1b-420d-bb57-f5aa0d692ba8');
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10004, 4, null, null, null, null, null);

    @Autowired
    SeatReserveRepository seatReserveRepository;

    @Autowired
    SeatHoldDetailRepository seatHoldDetailRepository;

    @Autowired
    TicketService ticketService;

    @Test
    public void numSeatsAvailable() {
        //two seats never been held and one seat holding expired
        assertEquals(3, ticketService.numSeatsAvailable());
    }

    @Test
    public void findAndHoldSeatsWithInvalidNumSeats() {
        String customerEmail = "ticket@icebreaker.com";
        try {
            ticketService.findAndHoldSeats(-3, customerEmail);
            Assert.fail();
        } catch (TicketException e) {
            assertEquals("Number of seats can't be 0 or negative number", e.getMessage());
        }

        try {
            ticketService.findAndHoldSeats(0, customerEmail);
            Assert.fail();
        } catch (TicketException e) {
            assertEquals("Number of seats can't be 0 or negative number", e.getMessage());
        }
    }

    @Test
    public void findAndHoldSeatsWithInvalidEmail() {
        String customerEmail = "";
        try {
            ticketService.findAndHoldSeats(100, customerEmail);
            Assert.fail();
        } catch (TicketException e) {
            assertEquals("Customer email is required to hold the seats", e.getMessage());
        }
        customerEmail = " ";
        try {
            ticketService.findAndHoldSeats(100, customerEmail);
            Assert.fail();
        } catch (TicketException e) {
            assertEquals("Customer email is required to hold the seats", e.getMessage());
        }

        customerEmail = null;
        try {
            ticketService.findAndHoldSeats(100, customerEmail);
            Assert.fail();
        } catch (TicketException e) {
            assertEquals("Customer email is required to hold the seats", e.getMessage());
        }
    }

    @Test
    public void findAndHoldSeatsWithTooManySeats() {
        SeatHold seatHold = ticketService.findAndHoldSeats(100, "ticket@icebreaker.com");
        assertEquals(HoldStatus.HOLD_FAILED_NO_ENOUGH_SEATS, seatHold.getHoldstatus());
        assertNull(seatHold.getId());
    }

    @Test
    public void findAndHoldSeats() {
        String customerEmail = "ticket@icebreaker.com";
        SeatHold seatHold = ticketService.findAndHoldSeats(2, customerEmail);
        assertEquals(HoldStatus.HOLD_SUCCESS, seatHold.getHoldstatus());
        assertNotNull(seatHold.getId());
        assertEquals(2, seatHold.getSeatIds().size());
        assertEquals(customerEmail, seatHold.getCustomerEmail());
        assertEquals(10002, seatHold.getSeatIds().get(0).intValue());
        assertEquals(10004, seatHold.getSeatIds().get(1).intValue());

        //revert the change
        List<SeatReserve> seatReserves = seatReserveRepository.findByLastHoldId(seatHold.getId());
        for (SeatReserve seatReserve:seatReserves) {
            seatReserve.setLastHoldExpirationTime(null);
            seatReserve.setLastHoldId(null);
        }
        seatReserveRepository.saveAll(seatReserves);
    }

    @Test
    public void reserveSeats() {
        String customerEmail = "abc@icebreaker.com";
        SeatReserve seatReserve = seatReserveRepository.findById(2L).get();
        seatReserve.setLastHoldExpirationTime(new Date( System.currentTimeMillis() + 30000L)); //30 seconds later to expire
        seatReserveRepository.saveAndFlush(seatReserve);
        String reservedCode = ticketService.reserveSeats(1, customerEmail);
        assertTrue(StringUtils.isNotBlank(reservedCode));
        List<SeatReserve> reservedSeats = seatReserveRepository.findByLastHoldId(1);
        assertEquals(1, reservedSeats.size());
        assertEquals(customerEmail, reservedSeats.get(0).getReservedBy());
        assertEquals(reservedCode, reservedSeats.get(0).getReservedCode());

        //revert the change
        reservedSeats.get(0).setLastHoldExpirationTime(new Date(System.currentTimeMillis()-1000L));
        reservedSeats.get(0).setReservedTime(null);
        reservedSeats.get(0).setReservedCode(null);
        reservedSeats.get(0).setReservedBy(null);
        seatReserveRepository.saveAll(reservedSeats);
    }
}