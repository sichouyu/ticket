package com.icebreaker.ticket.service;

import com.icebreaker.SeatReservationApplication;
import com.icebreaker.ticket.entity.SeatHoldDetail;
import com.icebreaker.ticket.entity.SeatReserve;
import com.icebreaker.ticket.repository.SeatHoldDetailRepository;
import com.icebreaker.ticket.repository.SeatReserveRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SeatReservationApplication.class)
public class TicketCrudServiceTest {

//    insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
//    values(nextval('seq_seat_hold_detail'), 'allen@icebreaker.com', now(), 1);
//
//    insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
//    values(nextval('seq_seat_hold_detail'), 'steve@icebreaker.com', now(), 1);
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10001, 1, null, null, null, null, null);
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10002, 2, 1, now(), null, null, null);
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10003, 3, 2, now(), now(), 'steve@icebreaker.com', '20a01714-6d1b-420d-bb57-f5aa0d692ba8');
//
//    insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
//    values(nextval('seq_seat_reserve'), 10004, 4, null, null, null, null, null);

    @Autowired
    SeatReserveRepository seatReserveRepository;

    @Autowired
    SeatHoldDetailRepository seatHoldDetailRepository;

    @Autowired
    TicketCrudService ticketCrudService;

    @Test
    public void resetHoldSeats() {
        //First test case: if seat hold id is a valid id
        System.out.println(seatReserveRepository.findAll());
        int updateCount = ticketCrudService.resetHoldSeats(1);
        assertEquals(1, updateCount);
        assertEquals(0, seatReserveRepository.findByLastHoldId(1).size());
        SeatReserve seatReserve = seatReserveRepository.findById(2L).get();
        assertNull(seatReserve.getLastHoldExpirationTime());
        assertNull(seatReserve.getLastHoldId());

        //second test case: if seat hold id is not found
        int NOT_EXISTING_HOLD_ID = 99999;
        updateCount = ticketCrudService.resetHoldSeats(NOT_EXISTING_HOLD_ID);
        assertEquals(0, updateCount);

        //revert the changes
        seatReserve.setLastHoldExpirationTime(new Date());
        seatReserve.setLastHoldId(1);
        seatReserveRepository.save(seatReserve);
    }

    @Test
    public void holdSeats() {
        //Trying to hold 2 seats. One seat was never held before and one seat's holding has expired
        SeatReserve seatReserve = seatReserveRepository.findById(2L).get();
        seatReserve.setLastHoldExpirationTime(new Date(System.currentTimeMillis()-1000L)); //set holding expired
        seatReserveRepository.saveAndFlush(seatReserve);
        int updateCount = seatReserveRepository.holdSeats(3, 2, 30);
        assertEquals(2, updateCount);
        List<SeatReserve> holdSeats = seatReserveRepository.findByLastHoldId(3);
        assertEquals(2, holdSeats.size());
        assertEquals(10002, holdSeats.get(0).getSeatId().intValue());
        assertEquals(10004, holdSeats.get(1).getSeatId().intValue());

        //revert back the records
        holdSeats.get(0).setLastHoldExpirationTime(new Date());
        holdSeats.get(0).setLastHoldId(1);
        holdSeats.get(1).setLastHoldExpirationTime(null);
        holdSeats.get(1).setLastHoldId(null);
        seatReserveRepository.saveAll(holdSeats);
    }

    @Test
    public void addHoldRecord() {
        ticketCrudService.addHoldRecord("abc@icebreaker.com", 4);
        List<SeatHoldDetail> seatHolds = seatHoldDetailRepository.findAll();
        assertEquals(3, ((List) seatHolds).size());

        assertEquals("abc@icebreaker.com", seatHolds.get(2).getCustomerEmail());
        assertEquals(4, seatHolds.get(2).getNumberOfSeats().intValue());

        //revert the change
        seatHoldDetailRepository.deleteById(3L);
    }

    @Test
    public void reserveSeats() {
        String reservedCode = "ab024df-adjfdj-aaaa";
        String customerEmail = "abc@icebreaker.com";

        //First test case: reserve seat which have been expired
        int updateCount = ticketCrudService.reserveSeats(1, customerEmail, reservedCode);
        assertEquals(0, updateCount);

        //Second test case: reserve seats with valid seat hold id
        SeatReserve seatReserve = seatReserveRepository.findById(2L).get();
        seatReserve.setLastHoldExpirationTime(new Date( System.currentTimeMillis() + 30000L)); //30 seconds later to expire
        seatReserveRepository.saveAndFlush(seatReserve);
        updateCount = ticketCrudService.reserveSeats(1, customerEmail, reservedCode);
        assertEquals(1, updateCount);
        assertEquals(1, seatReserveRepository.findByLastHoldId(1).size());
        seatReserve = seatReserveRepository.findByLastHoldId(1).get(0);
        assertEquals(reservedCode, seatReserve.getReservedCode());
        assertEquals(customerEmail, seatReserve.getReservedBy());

        //Third test case: reserve seats with unknown seat hold id
        int UNKNOWN_HOLD_ID = 999999;
        updateCount = ticketCrudService.reserveSeats(UNKNOWN_HOLD_ID, customerEmail, reservedCode);
        assertEquals(0, updateCount);

        //Fourth test case: reserve seats which have been reserved
        updateCount = ticketCrudService.reserveSeats(2, customerEmail, reservedCode);
        assertEquals(0, updateCount);

        //revert the change
        seatReserve = seatReserveRepository.findByLastHoldId(1).get(0);
        seatReserve.setReservedBy(null);
        seatReserve.setReservedCode(null);
        seatReserve.setReservedTime(null);
        seatReserveRepository.saveAndFlush(seatReserve);
    }
}