package com.icebreaker.ticket.dto;

import com.icebreaker.ticket.constant.HoldStatus;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class SeatHoldTest {

    @Test
    public void toStringTest() {

        Date now = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
        List<Integer> seatIdList = new ArrayList<>(Arrays.asList(1,2,3));
        SeatHold seatHold = new SeatHold();
        seatHold.setCustomerEmail("abc@icebreaker.com");
        seatHold.setHoldTime(now);
        seatHold.setId(0);
        seatHold.setSeatIds(seatIdList);

        //Test case 1: hold status is null
        Assert.assertEquals("SeatHold [id=0, customerEmail=abc@icebreaker.com, holdTime=Tue Feb 11 00:00:00 EST 2014, holdStatus=null, seatIds=[1, 2, 3]]", seatHold.toString());

        seatHold.setHoldstatus(HoldStatus.HOLD_SUCCESS);
        Assert.assertEquals("SeatHold [id=0, customerEmail=abc@icebreaker.com, holdTime=Tue Feb 11 00:00:00 EST 2014, holdStatus=Seat holding finished successfully, seatIds=[1, 2, 3]]", seatHold.toString());

    }
}