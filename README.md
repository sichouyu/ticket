## Ticket reservation system

### Requirements

1. JDK 1.8+
2. Maven
3. (optional) Eclips or STS or IntelliJ

### Tools and frameworks 
1. Spring Boot micro service framework
2. H2 in memory database to simulate the storage layer

### How to execute the demo
1. Using maven to create jar file and run the junit tests
```coffeescript
mvn clean install
```
2. Start ticket reservation application
```coffeescript
java -jar {your_project_location}/ticket/target/ticket-0.0.1-SNAPSHOT.jar
```
The application will start as a web application with port 8080. 
You may use following URL to execute the demo.
```coffeescript
http://localhost:8080/ticket/demo
```
And you can use following URL to browse the data in the H2 database
```coffeescript
http://localhost:8080/h2-console/login.jsp
remember to use "jdbc:h2:mem:testdb" as JDBC URL value 
```

Data used for demo
##### table seat_hold_detail

```coffeescript
ID  	CUSTOMER_EMAIL  	HOLD_TIME  	            NUMBER_OF_SEATS  
1	allen@icebreaker.com	2018-07-26 01:21:45.895	    1
2	steve@icebreaker.com	2018-07-26 01:21:45.904	    1
```
##### Table seat_reserve

```coffeescript
D  	LAST_HOLD_EXPIRATION_TIME  	LAST_HOLD_ID  	PRIORITY  	RESERVED_BY  	        RESERVED_CODE  	                        RESERVED_TIME  	SEAT_ID  
1	null                        null            1	        null	                null	                                null	        10001
2	2018-07-26 01:21:45.906	    1               2	        null	                null	                                null	        10002
3	2018-07-26 01:21:45.907	    2	            3	        steve@icebreaker.com	20a01714-6d1b-420d-bb57-f5aa0d692ba8	2018-07-26 01:21:45.907	10003
4	null	                    null	    4	        null	                null	                                null	        10004
```

### What is inside demo

1. Original data contains 4 seat records. Seat 10001 and seat 10004 have never been held or reserved. Seat 10002 was held but expired. Seat 10003 was reserved.
2. Customer tries to hold two seats and succeeds. Both seat 10002 and seat 10004 are held due to priority level. The expiration time is set to be 10 seconds in property file.
3. Right after holding two seats, the same customer reserves the holding seats.
4. After the demo, except for seat 10001, all the other seats have been reserved.
5. If you happen to run demo more than once, the second time holding trial will fail because there is only one seat available.

### In concurrent requests mode, one tricky scenario can happen:
1. Customer A checks the available seats and finds there are enough seats. At the same time, customer B also checks the available seats and finds there are enough seats. However, available seats are not enough to satisfy both customers.
2. Whenever both customers try to hold the seats at the same time, the winning customer may successfully hold the seats. The loser customer may only hold partial seats. In this case, the loser customer may need to return these partial seats. Even if he/she fails to return them, these temporary holding seats will expire in next 10 seconds.


---

### /src/main/resources/application.properties

```properties
# Enabling H2 Console
spring.h2.console.enabled=true
#Turn Statistics on
spring.jpa.properties.hibernate.generate_statistics=true
logging.level.org.hibernate.stat=INFO
# Show all queries
spring.jpa.show-sql=true
spring.jpa.properties.hibernate.format_sql=true
logging.level.org.hibernate.type=INFO

#expiration time setting (seconds)
ticket.expiration.seconds=10
```
---

### /src/main/resources/data.sql

```sql
insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
values(nextval('seq_seat_hold_detail'), 'allen@icebreaker.com', now(), 1);

insert into seat_hold_detail (id, customer_email, hold_time, number_of_seats)
values(nextval('seq_seat_hold_detail'), 'steve@icebreaker.com', now(), 1);

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10001, 1, null, null, null, null, null);

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10002, 2, 1, now(), null, null, null);

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10003, 3, 2, now(), now(), 'steve@icebreaker.com', '20a01714-6d1b-420d-bb57-f5aa0d692ba8');

insert into seat_reserve (id, seat_id, priority, last_hold_id, last_hold_expiration_time, reserved_time, reserved_by, reserved_code)
values(nextval('seq_seat_reserve'), 10004, 4, null, null, null, null, null);
```
---

---
